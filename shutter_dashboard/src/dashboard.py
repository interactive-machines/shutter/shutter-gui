#!/usr/bin/env python

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dashboard.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

import rospy
from shutter_bringup.srv import StopMotors, StopMotorsRequest
from PyQt5 import QtCore, QtGui, QtWidgets
from std_msgs.msg import Bool
from sensor_msgs.msg import JointState
from diagnostic_msgs.msg import DiagnosticArray


class Ui_MainWindow(object):
    """
    Dashboard node. Displays a dashboard that shows the position, velocity, voltage, temperature, torque, and message
    of all four motors.
    """


    def __init__(self):
        rospy.init_node('dashboard', anonymous=True)
        super(Ui_MainWindow, self).__init__()
        self.gui_motors_disabled = False        # set to the state of the /motors_enabled topic in readMotorState
        self.read_motor = False
        self.diagnostics_order = []

        # subscribers
        rospy.Subscriber('/motors_enabled', Bool, self.readMotorState)
        rospy.Subscriber('/diagnostics', DiagnosticArray, self.recordDiagnostics)
        rospy.Subscriber('/joint_states', JointState, self.recordJointStates)

    def setupUi(self, MainWindow):
        """PyQt5 code to draw the GUI originally made in QT Designer"""
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(554, 270)
        MainWindow.setMinimumSize(QtCore.QSize(0, 0))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(114, 159, 207))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(238, 238, 236))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(114, 159, 207))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(238, 238, 236))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(114, 159, 207))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(238, 238, 236))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(238, 238, 236))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWindow.setPalette(palette)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.label_26 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_26.setFont(font)
        self.label_26.setObjectName("label_26")
        self.gridLayout.addWidget(self.label_26, 0, 2, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 6, 3, 1, 1)
        self.label_35 = QtWidgets.QLabel(self.centralwidget)
        self.label_35.setObjectName("label_35")
        self.gridLayout.addWidget(self.label_35, 4, 2, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 6, 1, 1, 1)
        self.label_25 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_25.setFont(font)
        self.label_25.setObjectName("label_25")
        self.gridLayout.addWidget(self.label_25, 0, 3, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 5, 3, 1, 1)
        self.label_33 = QtWidgets.QLabel(self.centralwidget)
        self.label_33.setObjectName("label_33")
        self.gridLayout.addWidget(self.label_33, 2, 2, 1, 1)
        self.label_21 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_21.setFont(font)
        self.label_21.setObjectName("label_21")
        self.gridLayout.addWidget(self.label_21, 1, 1, 1, 1)
        self.label_34 = QtWidgets.QLabel(self.centralwidget)
        self.label_34.setObjectName("label_34")
        self.gridLayout.addWidget(self.label_34, 3, 2, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setObjectName("label_10")
        self.gridLayout.addWidget(self.label_10, 4, 3, 1, 1)
        self.label_29 = QtWidgets.QLabel(self.centralwidget)
        self.label_29.setObjectName("label_29")
        self.gridLayout.addWidget(self.label_29, 1, 2, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 5, 1, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 6, 5, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_9.setFont(font)
        self.label_9.setObjectName("label_9")
        self.gridLayout.addWidget(self.label_9, 4, 1, 1, 1)
        self.label_28 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_28.setFont(font)
        self.label_28.setObjectName("label_28")
        self.gridLayout.addWidget(self.label_28, 0, 5, 1, 1)
        self.label_14 = QtWidgets.QLabel(self.centralwidget)
        self.label_14.setObjectName("label_14")
        self.gridLayout.addWidget(self.label_14, 3, 3, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.gridLayout.addWidget(self.label_7, 5, 5, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_13.setFont(font)
        self.label_13.setObjectName("label_13")
        self.gridLayout.addWidget(self.label_13, 3, 1, 1, 1)
        self.label_19 = QtWidgets.QLabel(self.centralwidget)
        self.label_19.setObjectName("label_19")
        self.gridLayout.addWidget(self.label_19, 2, 5, 1, 1)
        self.label_15 = QtWidgets.QLabel(self.centralwidget)
        self.label_15.setObjectName("label_15")
        self.gridLayout.addWidget(self.label_15, 3, 5, 1, 1)
        self.label_17 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_17.setFont(font)
        self.label_17.setObjectName("label_17")
        self.gridLayout.addWidget(self.label_17, 2, 1, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setObjectName("label_11")
        self.gridLayout.addWidget(self.label_11, 4, 5, 1, 1)
        self.label_18 = QtWidgets.QLabel(self.centralwidget)
        self.label_18.setObjectName("label_18")
        self.gridLayout.addWidget(self.label_18, 2, 3, 1, 1)
        self.label_30 = QtWidgets.QLabel(self.centralwidget)
        self.label_30.setObjectName("label_30")
        self.gridLayout.addWidget(self.label_30, 1, 4, 1, 1)
        self.label_22 = QtWidgets.QLabel(self.centralwidget)
        self.label_22.setObjectName("label_22")
        self.gridLayout.addWidget(self.label_22, 1, 3, 1, 1)
        self.label_23 = QtWidgets.QLabel(self.centralwidget)
        self.label_23.setObjectName("label_23")
        self.gridLayout.addWidget(self.label_23, 1, 5, 1, 1)
        self.label_41 = QtWidgets.QLabel(self.centralwidget)
        self.label_41.setObjectName("label_41")
        self.gridLayout.addWidget(self.label_41, 3, 4, 1, 1)
        self.label_37 = QtWidgets.QLabel(self.centralwidget)
        self.label_37.setObjectName("label_37")
        self.gridLayout.addWidget(self.label_37, 5, 2, 1, 1)
        self.label_36 = QtWidgets.QLabel(self.centralwidget)
        self.label_36.setObjectName("label_36")
        self.gridLayout.addWidget(self.label_36, 2, 4, 1, 1)
        self.label_38 = QtWidgets.QLabel(self.centralwidget)
        self.label_38.setObjectName("label_38")
        self.gridLayout.addWidget(self.label_38, 6, 2, 1, 1)
        self.label_27 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_27.setFont(font)
        self.label_27.setObjectName("label_27")
        self.gridLayout.addWidget(self.label_27, 0, 4, 1, 1)
        self.label_44 = QtWidgets.QLabel(self.centralwidget)
        self.label_44.setObjectName("label_44")
        self.gridLayout.addWidget(self.label_44, 4, 4, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 5, 4, 1, 1)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setObjectName("label_8")
        self.gridLayout.addWidget(self.label_8, 6, 4, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Umpush")
        font.setPointSize(11)
        self.pushButton.setFont(font)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 0, 0, 7, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 554, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.pushButton.clicked.connect(self.toggleMotors)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Shutter Dashboard"))
        self.label_26.setText(_translate("MainWindow", "Motor 1:"))
        self.label_3.setText(_translate("MainWindow", "Mes2"))
        self.label_35.setText(_translate("MainWindow", "Tem1"))
        self.label.setText(_translate("MainWindow", "Message:"))
        self.label_25.setText(_translate("MainWindow", "Motor 2:"))
        self.label_4.setText(_translate("MainWindow", "Tor2"))
        self.label_33.setText(_translate("MainWindow", "Vel1"))
        self.label_21.setText(_translate("MainWindow", "Position:"))
        self.label_34.setText(_translate("MainWindow", "Vol1"))
        self.label_10.setText(_translate("MainWindow", "Tem2"))
        self.label_29.setText(_translate("MainWindow", "Pos1"))
        self.label_2.setText(_translate("MainWindow", "Torque:"))
        self.label_5.setText(_translate("MainWindow", "Mes4"))
        self.label_9.setText(_translate("MainWindow", "Temperature:"))
        self.label_28.setText(_translate("MainWindow", "Motor 4:"))
        self.label_14.setText(_translate("MainWindow", "Vol2"))
        self.label_7.setText(_translate("MainWindow", "Tor4"))
        self.label_13.setText(_translate("MainWindow", "Voltage:"))
        self.label_19.setText(_translate("MainWindow", "Vel4"))
        self.label_15.setText(_translate("MainWindow", "Vol4"))
        self.label_17.setText(_translate("MainWindow", "Velocity:"))
        self.label_11.setText(_translate("MainWindow", "Tem4"))
        self.label_18.setText(_translate("MainWindow", "Vel2"))
        self.label_30.setText(_translate("MainWindow", "Pos3"))
        self.label_22.setText(_translate("MainWindow", "Pos2"))
        self.label_23.setText(_translate("MainWindow", "Pos4"))
        self.label_41.setText(_translate("MainWindow", "Vol3"))
        self.label_37.setText(_translate("MainWindow", "Tor1"))
        self.label_36.setText(_translate("MainWindow", "Vel3"))
        self.label_38.setText(_translate("MainWindow", "Mes1"))
        self.label_27.setText(_translate("MainWindow", "Motor 3:"))
        self.label_44.setText(_translate("MainWindow", "Tem3"))
        self.label_6.setText(_translate("MainWindow", "Tor3"))
        self.label_8.setText(_translate("MainWindow", "Mes3"))
        self.pushButton.setText(_translate("MainWindow", "Toggle Motors"))

    def recordJointStates(self, data):
        """Reads the /joint_states topic to update the position and velocity labels"""
        _translate = QtCore.QCoreApplication.translate

        # Position
        self.label_29.setText(_translate("MainWindow", str(round(data.position[data.name.index('joint_1')], 2))))
        self.label_22.setText(_translate("MainWindow", str(round(data.position[data.name.index('joint_2')], 2))))
        self.label_30.setText(_translate("MainWindow", str(round(data.position[data.name.index('joint_3')], 2))))
        self.label_23.setText(_translate("MainWindow", str(round(data.position[data.name.index('joint_4')], 2))))

        # Velocity
        self.label_33.setText(_translate("MainWindow", str(round(data.velocity[data.name.index('joint_1')], 2))))
        self.label_36.setText(_translate("MainWindow", str(round(data.velocity[data.name.index('joint_2')], 2))))
        self.label_18.setText(_translate("MainWindow", str(round(data.velocity[data.name.index('joint_3')], 2))))
        self.label_19.setText(_translate("MainWindow", str(round(data.velocity[data.name.index('joint_4')], 2))))

    def recordDiagnostics(self, data):
        """Reads the /diagnostics topic to update the voltage, temperature, torque, and message labels"""
        _translate = QtCore.QCoreApplication.translate

        # Records the order of the joints in the diagnostics message for future use
        if len(self.diagnostics_order) <= 4:
            self.diagnostics_order.append(data.status[1].name)
            self.diagnostics_order.append(data.status[2].name)
            self.diagnostics_order.append(data.status[3].name)
            self.diagnostics_order.append(data.status[4].name)

        # Voltage
        self.label_34.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_1') + 1].values[2].value)))
        self.label_14.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_2') + 1].values[2].value)))
        self.label_41.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_3') + 1].values[2].value)))
        self.label_15.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_4') + 1].values[2].value)))

        # Temperature
        self.label_35.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_1') + 1].values[1].value)))
        self.label_10.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_2') + 1].values[1].value)))
        self.label_44.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_3') + 1].values[1].value)))
        self.label_11.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_4') + 1].values[1].value)))

        # Torque
        self.label_37.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_1') + 1].values[5].value)))
        self.label_4.setText(_translate("MainWindow",
                                        str(data.status[self.diagnostics_order.index('joint_2') + 1].values[5].value)))
        self.label_6.setText(_translate("MainWindow",
                                        str(data.status[self.diagnostics_order.index('joint_3') + 1].values[5].value)))
        self.label_7.setText(_translate("MainWindow",
                                        str(data.status[self.diagnostics_order.index('joint_4') + 1].values[5].value)))

        # Message
        self.label_38.setText(_translate("MainWindow",
                                         str(data.status[self.diagnostics_order.index('joint_1') + 1].message)))
        self.label_3.setText(_translate("MainWindow",
                                        str(data.status[self.diagnostics_order.index('joint_2') + 1].message)))
        self.label_8.setText(_translate("MainWindow",
                                        str(data.status[self.diagnostics_order.index('joint_3') + 1].message)))
        self.label_5.setText(_translate("MainWindow",
                                        str(data.status[self.diagnostics_order.index('joint_4') + 1].message)))

        # Turn text red if torque changes from 'ON'
        if str(data.status[self.diagnostics_order.index('joint_1') + 1].values[5].value) == 'ON':
            self.label_37.setStyleSheet('color: black')
        else:
            self.label_37.setStyleSheet('color: red')
        if str(data.status[self.diagnostics_order.index('joint_2') + 1].values[5].value) == 'ON':
            self.label_4.setStyleSheet('color: black')
        else:
            self.label_4.setStyleSheet('color: red')
        if str(data.status[self.diagnostics_order.index('joint_3') + 1].values[5].value) == 'ON':
            self.label_6.setStyleSheet('color: black')
        else:
            self.label_6.setStyleSheet('color: red')
        if str(data.status[self.diagnostics_order.index('joint_4') + 1].values[5].value) == 'ON':
            self.label_7.setStyleSheet('color: black')
        else:
            self.label_7.setStyleSheet('color: red')

        # Turn text red if message changes from 'OK'
        if str(data.status[self.diagnostics_order.index('joint_1') + 1].message) == 'OK':
            self.label_38.setStyleSheet('color: black')
        else:
            self.label_38.setStyleSheet('color: red')
        if str(data.status[self.diagnostics_order.index('joint_2') + 1].message) == 'OK':
            self.label_3.setStyleSheet('color: black')
        else:
            self.label_3.setStyleSheet('color: red')
        if str(data.status[self.diagnostics_order.index('joint_3') + 1].message) == 'OK':
            self.label_8.setStyleSheet('color: black')
        else:
            self.label_8.setStyleSheet('color: red')
        if str(data.status[self.diagnostics_order.index('joint_4') + 1].message) == 'OK':
            self.label_5.setStyleSheet('color: black')
        else:
            self.label_5.setStyleSheet('color: red')

    def readMotorState(self, data):
        """This function, which is only run once on init sets gui_motors_disabled to the current state of the motors"""
        if not self.read_motor:
            _translate = QtCore.QCoreApplication.translate
            self.gui_motors_disabled = data.data
            if self.gui_motors_disabled:
                self.pushButton.setText(_translate("MainWindow", "Disable Motors"))
            else:
                self.pushButton.setText(_translate("MainWindow", "Enable Motors"))
            self.read_motor = True

    def toggleMotors(self):
        """This function which is activated when the button is pushed uses the motor_stopper service to toggle motors"""
        _translate = QtCore.QCoreApplication.translate
        rospy.wait_for_service('motor_stopper')

        turn_off_motors = rospy.ServiceProxy('motor_stopper', StopMotors)
        turn_off_motors(self.gui_motors_disabled)

        if self.gui_motors_disabled:
            print("Disabling the motors")
            self.gui_motors_disabled = False
            self.pushButton.setText(_translate("MainWindow", "Enable Motors"))
        else:
            print("Enabling the motors")
            self.gui_motors_disabled = True
            self.pushButton.setText(_translate("MainWindow", "Disable Motors"))


if __name__ == "__main__":
    try:
        import sys
        app = QtWidgets.QApplication(sys.argv)
        MainWindow = QtWidgets.QMainWindow()
        ui = Ui_MainWindow()
        ui.setupUi(MainWindow)
        MainWindow.show()
        sys.exit(app.exec_())
    except rospy.ROSInterruptException:
        pass


