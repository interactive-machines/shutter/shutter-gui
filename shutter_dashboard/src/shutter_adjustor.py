#!/usr/bin/env python
# Node to move the robot with a controller.
# Temporary, made for Tim

import rospy
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState

class MotorControlNode(object):

    def __init__(self):
        rospy.init_node('shutter_controller', anonymous=True)

        # params
        self.motor_poses = [0.0, 0.0, 0.0, 0.0]
        self.motor_maxes = (2.55, 1.55, 1.55, 1.55)
        self.motor_changes = [0.0, 0.0, 0.0, 0.0]
        self.move_factor = 0.015
        self.current_pose = []
        self.current_name = []
        self.read_pose = False
        self.adjust_pose = False

        # publishers
        self.pub1 = rospy.Publisher('/joint_1/command', Float64, queue_size=10)
        #self.pub2 = rospy.Publisher('/joint_2/command_safe', Float64, queue_size=10)
        self.pub3 = rospy.Publisher('/joint_3/command', Float64, queue_size=10)
        #self.pub4 = rospy.Publisher('/joint_4/command_safe', Float64, queue_size=10)

        # subscribers
        rospy.Subscriber('joy', Joy, self.update_pose, queue_size=1)
        rospy.Subscriber('/joint_states', JointState, self.record_pose)

    def record_pose(self, data):
        self.current_pose = data.position
        self.current_name = data.name

    def update_pose(self, joy_msg):
        if joy_msg.buttons[7] == 1:
            if joy_msg.buttons[6] == 1:
                self.motor_changes[1] = joy_msg.axes[1] * self.move_factor
                self.motor_changes[3] = joy_msg.axes[4] * self.move_factor
                self.motor_changes[0] = 0.0
                self.motor_changes[2] = 0.0

            else:
                self.motor_changes[0] = joy_msg.axes[0] * self.move_factor
                self.motor_changes[2] = joy_msg.axes[4] * self.move_factor
                self.motor_changes[1] = 0.0
                self.motor_changes[3] = 0.0
        else:
            self.motor_changes[0] = 0.0
            self.motor_changes[1] = 0.0
            self.motor_changes[2] = 0.0
            self.motor_changes[3] = 0.0

        if joy_msg.axes[0] == 0 and joy_msg.axes[4] == 0 and not joy_msg.buttons[7] == 1:
            self.adjust_pose = False
        else:
            self.adjust_pose = True
        #self.pub4.publish(self.motor_poses[3])



    def test(self):
        # Check if any of the motors are over extended
        rospy.wait_for_message('/joint_states', JointState)
        if not self.read_pose:
            self.motor_poses[0] = self.current_pose[self.current_name.index('joint_1')]
            self.motor_poses[2] = self.current_pose[self.current_name.index('joint_3')]
            self.read_pose = True

        for num in range(4):
            self.motor_poses[num] = self.motor_poses[num] + self.motor_changes[num]
            if self.motor_poses[num] > self.motor_maxes[num]:
                self.motor_poses[num] = self.motor_maxes[num]
            elif self.motor_poses[num] < -1 * self.motor_maxes[num]:
                self.motor_poses[num] = -1 * self.motor_maxes[num]
        print(self.motor_poses)
        self.pub1.publish(self.motor_poses[0])
        # self.pub2.publish(self.motor_poses[1])
        self.pub3.publish(self.motor_poses[2])

def main():
    motor_control_node = MotorControlNode()

    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        while motor_control_node.adjust_pose:
            motor_control_node.test()
            rate.sleep()
        motor_control_node.read_pose = False

if __name__ == '__main__':
    main()