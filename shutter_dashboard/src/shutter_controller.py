#!/usr/bin/env python
# Node to move the robot with a controller.

import rospy
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64, Bool
from sensor_msgs.msg import JointState


class MotorControlNode(object):
    """Node to move the robot using a controller"""

    def __init__(self):
        rospy.init_node('shutter_controller', anonymous=True)

        # params
        self.motor_poses = [0.0, 0.0, 0.0, 0.0]
        self.motor_maxes = (2.55, 1.55, 1.55, 1.55)
        self.motor_changes = [0.0, 0.0, 0.0, 0.0]
        self.move_factor = 0.015
        self.read_pose = False
        self.current_pose = []
        self.current_name = []
        self.adjusting = False
        self.motors_enabled = True

        # publishers
        self.pub1 = rospy.Publisher('/joint_1/command_safe', Float64, queue_size=10)
        self.pub2 = rospy.Publisher('/joint_2/command_safe', Float64, queue_size=10)
        self.pub3 = rospy.Publisher('/joint_3/command_safe', Float64, queue_size=10)
        self.pub4 = rospy.Publisher('/joint_4/command_safe', Float64, queue_size=10)

        # subscribers
        rospy.Subscriber('joy', Joy, self.update_pose, queue_size=1)
        rospy.Subscriber('/joint_states', JointState, self.record_pose)
        rospy.Subscriber('/motors_enabled', Bool, self.check_enabled)

    def check_enabled(self, data):
        # Check if motors have been enabled or disabled
        if data.data:
            self.motors_enabled = True
        else:
            self.motors_enabled = False

    def record_pose(self, data):
        """
        When the robot isn't being operated its current position is recorded so the controller node is aware of
        movements caused by other scripts
        """
        self.current_pose = data.position
        self.current_name = data.name
        if not self.read_pose:
            self.motor_poses[0] = self.current_pose[self.current_name.index('joint_1')]
            self.motor_poses[1] = self.current_pose[self.current_name.index('joint_2')]
            self.motor_poses[2] = self.current_pose[self.current_name.index('joint_3')]
            self.motor_poses[3] = self.current_pose[self.current_name.index('joint_4')]
            self.read_pose = True

    def update_pose(self, joy_msg):
        """Use joystick input to update how much the motors should be changing"""
        if joy_msg.buttons[7] == 1 and joy_msg.buttons[6] != 1:
            self.motor_changes[2] = joy_msg.axes[1] * self.move_factor
            self.motor_changes[3] = joy_msg.axes[4] * self.move_factor
            self.motor_changes[0] = 0.0
            self.motor_changes[1] = 0.0
        elif joy_msg.buttons[6] == 1 and joy_msg.buttons[7] != 1:
            self.motor_changes[0] = joy_msg.axes[0] * self.move_factor
            self.motor_changes[1] = joy_msg.axes[4] * self.move_factor
            self.motor_changes[2] = 0.0
            self.motor_changes[3] = 0.0
        else:
            self.motor_changes[0] = 0.0
            self.motor_changes[1] = 0.0
            self.motor_changes[2] = 0.0
            self.motor_changes[3] = 0.0
        if joy_msg.buttons[7] == 0 and joy_msg.buttons[6] == 0:
            self.adjusting = False


        else:
            self.adjusting = True

    def move(self):
        """Check if any of the motors are over extended and then updates the positions with the change in position"""
        # Checks if motor_poses are not 0 because the program takes a moment to load
        # the current position when launched and shouldn't publish commands until it loads
        if self.motors_enabled and not self.motor_poses[0] == 0.0 and not self.motor_poses[1] == 0.0:
            for num in range(4):
                self.motor_poses[num] = self.motor_poses[num] - self.motor_changes[num]
                if self.motor_poses[num] > self.motor_maxes[num]:
                    self.motor_poses[num] = self.motor_maxes[num]
                elif self.motor_poses[num] < -1 * self.motor_maxes[num]:
                    self.motor_poses[num] = -1 * self.motor_maxes[num]
            print(self.motor_poses)
            self.pub1.publish(self.motor_poses[0])
            self.pub2.publish(self.motor_poses[1])
            self.pub3.publish(self.motor_poses[2])
            self.pub4.publish(self.motor_poses[3])


def main():
    motor_control_node = MotorControlNode()

    rate = rospy.Rate(60)
    while not rospy.is_shutdown():
        if motor_control_node.adjusting:
            motor_control_node.move()
            rate.sleep()
        else:
            motor_control_node.read_pose = False


if __name__ == '__main__':
    main()