Shutter GUI Repository
======================

GUIs for controlling and visualizing shutter data/status. Dependent on the shutter_bringup package.

## Quick Start

To launch the GUI execute the following:

```bash
$ roslaunch shutter_dashboard shutter_dashboard.launch
```

By default, the Arbotix driver will try to connect to the real robot. If you wish to run in simulation, launch the dashboard with the following paramater:

```bash
$ roslaunch shutter_dashboard shutter_dashboard.launch simulation:=true
```

In addition to the robot drivers, the dashboard and Arbotix GUI will be launched. You can use the dashboard to visualize information about each of the joints and enable or disable the motors. Disabling the motors will pause the Arbotix GUI and stop commands published to the /joint_X/command_safe topics from going through. 


## Moving the Robot with a Controller

The robot can be operated by a controller by executing the following:

```bash
$ roslaunch shutter_dashboard shutter_controller.launch
```

In addition to launching the dashboard, the robot can be moved now via the Joy node. While the left trigger is held, moving the left stick horizontally will move joint one and moving the right stick vertically will move joint 2. While the right trigger is held, moving the left stick vertically will move joint 3 and moving the right stick wilil move joint 4. Give the robot a few seconds to load after launching before operating.

